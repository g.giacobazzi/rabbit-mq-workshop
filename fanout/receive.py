#!/usr/bin/env python
import pika, sys, os

def main(consumer_queue):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    result = channel.queue_declare(queue='', durable=False, auto_delete=True)

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)

    channel.queue_bind(exchange='ex-fan', queue=result.method.queue)
    channel.basic_consume(queue='', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        main(sys.argv[1])
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)