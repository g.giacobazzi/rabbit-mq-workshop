#!/usr/bin/env python
import pika
import time

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()


index_count = 0
while True:
    #channel.queue_declare(queue=f'hello{index_count}')
    channel.basic_publish(exchange='ex-fan', routing_key=f'hello{index_count}', body=f'Hello World ${index_count}!')
    print(f" [x] Sent 'Hello World ${index_count}!'")
    time.sleep(1)
    index_count+=1
    if index_count == 3:
        index_count = 0
connection.close()