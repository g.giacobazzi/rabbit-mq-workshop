#!/usr/bin/env python
import pika
import time
import random

name = ["gino", "pino"]
type = ["type1", "type2"]
event = ["login", "logout", ""]
body = ["error", "ciao"]

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

index_count = 0
while True:
    #channel.queue_declare(queue=f'hello{index_count}')
    routing_key = ".".join(
            [
                random.choice(name),
                random.choice(type),
                random.choice(event),
            ]
        ).rstrip(".")

    print(routing_key)
    message = f'Hello World ${index_count}!'.join(random.choice(body))
    print(f"Sent {message}")
    channel.basic_publish(exchange='ex-topic', routing_key=routing_key, body=message)
    time.sleep(1)
    index_count+=1
    if index_count == 3:
        index_count = 0
connection.close()