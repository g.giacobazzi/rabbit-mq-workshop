#!/usr/bin/env python
import pika, sys, os

def on_message(channel, method_frame, header_frame, body):
    print(body)
    if "error" in str(body):
        channel.basic_nack(delivery_tag=method_frame.delivery_tag, requeue=not method_frame.redelivered)
    else:
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)


def main(consumer_queue):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)

    channel.basic_consume(queue=consumer_queue, on_message_callback=on_message)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        main(sys.argv[1])
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)